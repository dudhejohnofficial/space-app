package id.nasa.spaceapp.di

import org.koin.core.KoinComponent

const val SERVER_URL = "https://tracker.whatsdare.com"
// const val SERVER_URL = "http://192.168.0.163:1337"
const val TIME_OUT = 60L

/**
 * App setting class where can keep constant and do network calls
 * */
class Settings : KoinComponent {

    private var token: String? = null

    /**
     * This method returns the updated user token
     * */
    fun getAccessToken() = token

    /**
     * method used to update the token once it's refreshed
     * */
    fun setAccessToken(token: String?) {
        this.token = token
    }
}
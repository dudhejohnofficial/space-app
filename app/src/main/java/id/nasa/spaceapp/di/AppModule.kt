package id.nasa.spaceapp.di

import id.nasa.spaceapp.main.view.fragments.MobileNumberViewModel
import id.nasa.spaceapp.preference.PreferenceManager
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

/**
 * ScopeID for the current logged in scope
 */
const val NASA_SCOPE_ID = "LOGGED_IN"

/**
 * Qualifier for the logged in scope
 */
val NASA_SCOPE_QUALIFIER = named(NASA_SCOPE_ID)

/**
 * App Components
 */
val viewModelModule = module {

    scope(NASA_SCOPE_QUALIFIER) {
        viewModel {
            MobileNumberViewModel()
        }
    }
}

val useCaseModule = module {
    scope(NASA_SCOPE_QUALIFIER) {

    }
}

val constantModules = module {
    scope(NASA_SCOPE_QUALIFIER) {

    }
}

val repositoryModule = module {

    scope(NASA_SCOPE_QUALIFIER) {

    }
}

val roomDataSourceModule = module {

    single { PreferenceManager(androidContext()) }

}

// Gather all app modules
val nasaApp = listOf(
    viewModelModule,
    constantModules, useCaseModule, remoteDataSourceModule, repositoryModule, roomDataSourceModule
)
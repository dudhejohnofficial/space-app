package id.nasa.spaceapp.main.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import id.nasa.spaceapp.main.view.OtpFragment
import id.nasa.spaceapp.main.view.fragments.MobileNumberFragment

class PagerAdapter(manager: FragmentManager) :
    FragmentPagerAdapter(manager, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> MobileNumberFragment.newInstance()
            1 -> OtpFragment.newInstance()
            else -> Fragment()
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return "Login"
    }
}

package id.nasa.spaceapp.main.view.fragments

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.nasa.spaceapp.R

class MobileNumberFragment : Fragment() {

    companion object {
        fun newInstance() = MobileNumberFragment()
    }

    private lateinit var viewModel: MobileNumberViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.mobile_number_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MobileNumberViewModel::class.java)
        // TODO: Use the `ViewModel`
    }

}

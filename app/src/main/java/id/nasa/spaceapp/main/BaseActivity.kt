package id.nasa.spaceapp.main

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import id.nasa.spaceapp.R
import id.nasa.spaceapp.di.NASA_SCOPE_ID
import id.nasa.spaceapp.di.NASA_SCOPE_QUALIFIER
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.Job
import org.koin.android.ext.android.getKoin
import org.koin.core.scope.Scope
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

/**
 * Abstract Activity that has a coroutine scope that lives between
 */
abstract class BaseActivity(
    private val parentContext: CoroutineContext = EmptyCoroutineContext
) : AppCompatActivity() {

    protected lateinit var coroutineScope: CoroutineScope
        private set

    val viewModelScope: Scope? get() = _currentScope

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        coroutineScope =
            CoroutineScope(Dispatchers.Main + parentContext + SupervisorJob(parentContext[Job]))
    }

    override fun onDestroy() {
        coroutineScope.cancel()
        super.onDestroy()
    }

    private fun internalCreateScope(): Scope? {
        val created = getKoin().getScopeOrNull(NASA_SCOPE_ID) == null
        val scope = getKoin()
            .getOrCreateScope(NASA_SCOPE_ID, NASA_SCOPE_QUALIFIER)

        if (created) {
            scope.declare(this)
        }

        return scope
    }

    private var _currentScope = internalCreateScope()

    /**
     * Destroy/close out a logged in scope. Should only be called when a user is logging out
     */
    fun destroyLoggedInScope() {
        _currentScope.also { scope ->
            _currentScope = null
            scope?.close()
        }
    }

    /**
     * Start a new logged in scope.  If one is already active then it is returned without creation
     */
    fun startLoggedInScope(): Scope? = internalCreateScope().apply {
        _currentScope = this
    }

    protected fun showErrorMessage(message: String, view: View) {
        val snack = Snackbar.make(
            view,
            message,
            Snackbar.LENGTH_INDEFINITE
        )
        snack.setAction(resources.getString(R.string.ok)) {
            snack.dismiss()
        }
        snack.show()
    }
}
package id.nasa.spaceapp.preference

import android.content.Context
import android.content.SharedPreferences
import org.koin.core.KoinComponent

const val PREF_PARAM_TOKEN = "token"
const val PREF_PARAM_USER_ID = "userId"

/**
 * Preference manager class to store and retrieve data
 * @param context is required to the shared Preference
 * */
class PreferenceManager(context: Context) : KoinComponent {

    private val preferences: SharedPreferences =
        context.getSharedPreferences("prefs", Context.MODE_PRIVATE)


    /**
     * Used to store the string the values in preference
     * @param [key] preference key used to store values
     * @param [id] preference value
     * */
    fun storeValue(key: String, id: String) {
        preferences.edit().putString(key, id).apply()
    }

    /**
     * Used to get the string the values from preference
     * @param [key] preference key used to store values
     */
    fun getValue(key: String): String? =
        preferences.getString(key, "")

    private fun storeStringSet(key: String, list: MutableSet<String>) {
        val set: MutableSet<String> = HashSet()
        set.addAll(list)
        preferences.edit().putStringSet(key, set).apply()
    }

    private fun getStringSet(key: String): MutableSet<String>? =
        preferences.getStringSet(key, null)
}
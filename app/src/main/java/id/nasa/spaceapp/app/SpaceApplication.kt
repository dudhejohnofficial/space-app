package id.nasa.spaceapp.app

import android.app.Application
import id.nasa.spaceapp.di.nasaApp
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.core.logger.Level

class SpaceApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        stopKoin()
        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@SpaceApplication)
            modules(nasaApp)
        }
    }
}